import { render, screen } from '@testing-library/react';
import App from './App';
import Create from './components/createStudent/createStudent.js'

// test wheter the Title renders
test('renders students create and show bar', () => {
  render(<App />);
  const linkElement = screen.getByText(/Students Register/i);
  expect(linkElement).toBeInTheDocument();
});
