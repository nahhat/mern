import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import axios from 'axios';
import {useEffect, useState} from 'react';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';


// Create showStudent component to display student Register
export default function ShowStudents() {

  // decalre use state 
  const [studentsList, setStudnetList] = useState([])

  // delete student by id from the database
  const deleteStudent = (id) => {
    axios.delete(`http://localhost:5000/students/${id}`).then( () => {
      window.location.reload(false);
    }  )
  }
  // get student to display student register
  useEffect(() => {
    axios.get('http://localhost:5000/students').then( (allStudents) => {
      setStudnetList(allStudents.data);
    })
  }, [])
 
  // Created student register display format
  return (
    <>
    <h2>Student Database</h2>
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell align="right">Registration Number</TableCell>
            <TableCell align="right">Grade</TableCell>
            <TableCell align="right">Section</TableCell>
            <TableCell align="right">Action</TableCell>

          </TableRow>
        </TableHead>
        <TableBody>
          {studentsList.map((student, key) => (
            <TableRow
              key={key}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {student.studentName}
              </TableCell>
              <TableCell align="right">{student.regNo}</TableCell>
              <TableCell align="right">{student.grade}</TableCell>
              <TableCell align="right">{student.section}</TableCell>
              <TableCell align="right">

                {/* delete the selected student when bin icon is clicked */}
                <IconButton aria-label="delete" size="small" onClick={() => deleteStudent(student._id)}>
                  <DeleteIcon fontSize="small" />
                </IconButton>
              </TableCell>

            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </>
  );
}
