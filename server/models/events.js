const mongoose = require( 'mongoose');

// create a model for logged request
const eventSchema = mongoose.Schema({
    
    timestamp: Date,
    rawHeaders: [ String ],
    httpVersion: String,
    method: String,
    remoteAddress: String,
    remoteFamily: String,
    url: String

});
// enforce a scheme on the moodel
const event = mongoose.model('event', eventSchema);
module.exports = event;