const mongoose =require( 'mongoose');

// create a studnet model
const studentSchema = mongoose.Schema({
    regNo: Number,
    studentName: String,
    grade: String,
    section: {
        type: String,
        default: 'A'
    }

});

// enmforce a schema on the model
const student = mongoose.model('student', studentSchema);
module.exports = student;