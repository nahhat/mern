const mongoose = require('mongoose') ;
const bcrypt = require('bcryptjs') ;
const jwt = require('jsonwebtoken') ;

const JWT_EXPIRE = "10min";

const JWT_SECRET = "10486c686966fdf4534d75cd187f9a18fb5c83c72802e7724c545cf83a330edaa5ca192";

// create an admin model
const UserSchema = mongoose.Schema({
    
    // username is required and must be unique
    username: {
        type: String,
        required: [true, "Pleasse provide a username"],
        unique: true
    },
    // email must be unique, is required and must pattern match
    email: {
        type: String,
        required: [true, "Please provide an email address"],
        unique: true,
        match: [/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 
                "Please provide a valid email address"]
    },
    // password must have a min length of 8, contain at least 1 lower and uppercase character, 1 number and 1 symbol
    password: {
        type: String, 
        required: true,
        minlength: 8,
        amtch: [/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/, "Password must contain at least 8 characters, 1 capital letter, 1 lower case letter, 1 number and 1 symbol"],
        select: false
    }

});

// hash the password using salt
UserSchema.pre("save", async function(next) {
    if(!this.isModified("password")){
        next();
    }
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
    next();
});
 // compare passwords
UserSchema.methods.comparePasswords = async function(password){
    return await bcrypt.compare(password, this.password);
}
 // obtain signed token
UserSchema.methods.getSignedToken = function() {
    return jwt.sign({id: this._id}, JWT_SECRET, { 
        expiresIn: JWT_EXPIRE});
}

// enforce a schema on the model
const user = mongoose.model('user', UserSchema);
module.exports = user;