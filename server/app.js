const cors = require('cors') ;
const express = require('express') ;
const bodyParser = require('body-parser') ;
const studentRoutes = require( './routes/student.js');
const authRoutes = require('./routes/auth.routes.js') ;
const eventRoutes = require('./routes/event.route.js') ;

// initialise express
const app = express();
// allow cross origin resouce sharing
app.use(cors());

// parse urlencoded data to json
app.use(bodyParser.json({limit: "20mb", extended:true}));
app.use(bodyParser.urlencoded({limit: "20mb", extended:true}));

// for each path use corresponding router
app.use('/api/auth', authRoutes);
app.use('/students', studentRoutes);
app.use('/api/events', eventRoutes);

// export express app
module.exports = app