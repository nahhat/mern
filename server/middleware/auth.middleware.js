const jwt = require('jsonwebtoken') ;
const { post } = require('superagent');
const UserSchema = require( '../models/users.js');
const JWT_SECRET = "10486c686966fdf4534d75cd187f9a18fb5c83c72802e7724c545cf83a330edaa5ca192";

// protect the log request route middleware
exports.protect = async (req, res, next) => {
    let token; 

    // extract token form the req headers
    if(req.headers.authorization && req.headers.authorization.startsWith("Bearer")){
        token = req.headers.authorization.split(" ")[1];

    }
    // if no token present return unauthorised
    if(!token){
        res.status(401).json({success: false, error: "not authorised to access this route"})
    }

    try {// veryfi token if present
        const decoded = jwt.verify(token, JWT_SECRET);
        // check user in the admin database
        const user = await UserSchema.findById(decoded.id);
        // return 404 not found if no user is found
        if(!user){
            res.status(404).json({success: false, error: "No user found with this id"})
        }
        
        req.user = user;
        // pass on to server
        next();
    } catch (error) {

        //this line heare works but throws an error needs fixing before submission
        //issue is due to line 15 sending a response to client before line 34.
        //if it stops executing after the if statement error should be avoided
        //return statements might work
        //when use return code executions should stop
        res.status(401).json({
            success:false,
            error: error.message
        })
        console.log(error.message);
    }
}