const EventSchema =require( '../models/events.js');

// log request middleware
exports.logRequest = async (req, res, next) => {
    const { rawHeaders, httpVersion, method, socket, url } = req;
    const { remoteAddress, remoteFamily } = socket;
    
    // sent event model
    const event = {
          timestamp: Date.now(),
          rawHeaders,
          httpVersion,
          method,
          remoteAddress,
          remoteFamily,
          url
        }

        // add new event ot event schema
    const newEvent = new EventSchema(event);
    try {

        // save event in database
        await newEvent.save();
        console.log('Request Logged !!!');

        // forward reqquest
        next();
    } catch (error) {
        console.log(`Request logging failed!! ${error}`);
    }
}