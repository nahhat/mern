const UserSchema = require('../models/users.js') ;

// implement admin register function
const register = async (req, res, next) => {
    const user = req.body;
    const newUser = new UserSchema(user);
    //add newu admnn to databse
    try {
        await newUser.save();

        //assing jwt token to admin
        sendToken(newUser, 201, res);

        // catch error
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message
        });
    }

};

// implement login functionality for asmin accouint
const login = async (req, res, next) => {
    const {email, password} = req.body;

    // enforce email and password input
    if(!email || !password){
        res.status(400).json({ success: false, error: "Please provide email and password"});
    }

    try {
        // check given email has an existing admin account
        const user = await UserSchema.findOne( {email} ).select("+password");
        // if account doesnt exist deny access
        if(!user){
            res.status(404).json({ success: false, error: "No account with such email has been registered"});
        };

        // if credentials dont match to database deny access
        const isMatch = await user.comparePasswords(password);
        if(!isMatch){
            res.status(404).json({ success: false, error: "invalid Credentials!"});
        };

        // if no rules are breached assign token
        sendToken(user, 200, res);

        // catch error
    } catch (error) {
        res.status(500).json({success: false, error: error.message});
    }
};
 // sent token function
const sendToken = (user, statusCode, res) => {
    const token = user.getSignedToken();
    res.status(statusCode).json({success: true, token})
}
module.exports = {register, login}