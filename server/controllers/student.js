const StudentData = require( '../models/student.js');

// get students from the database method
exports.getStudents = async (req, res) => {

    try {
        const allSttudents = await StudentData.find();
        // return data as json
        res.status(200).json(allSttudents);
    } catch (error) {
        res.status(404).json({message: error.message});
    }
}

// post new student to databse
exports.createStudent = async (req, res) => {

    const student = req.body;
    const newStudent = new StudentData(student);

    try {
        await newStudent.save();
        // set neew student as response in Json
        res.status(201).json(newStudent);
    } catch (error) {
        res.status(409).json({ message: error.message});
    }
}
// delete existing student by id
exports.deleteStudent = async (req, res) => {

    const id = req.params.id;

    try {
        // find and remove student by id
        await StudentData.findByIdAndRemove(id).exec();
        res.send('Successefully Deleted!');
    } catch (error) {
        console.log(error);
    }
}