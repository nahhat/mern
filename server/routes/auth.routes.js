const express = require('express') ;
const { register, login } = require('../controllers/auth.controllers.js') ;
const { logRequest } = require('../middleware/event.middleware.js') ;

const router = express.Router();
// use oruter with logRequest middleware for post to /register
router.post('/register', logRequest, register);
// use oruter with logRequest middleware for post to /login
router.post('/login', logRequest, login);
// export router
module.exports = router;