const express = require('express') ;
const { getReqLogs } = require('../controllers/event.controllers.js') ;
const { protect } = require('../middleware/auth.middleware.js') ;
const { logRequest } = require('../middleware/event.middleware.js') ;

const router = express.Router();
// protect and long get requests when attemoting to access logged Requests
router.get("/", logRequest, protect, getReqLogs);

// export router
module.exports = router;