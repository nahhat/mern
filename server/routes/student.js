const express = require('express') ;
const { getStudents, createStudent, deleteStudent } = require('../controllers/student.js') ;
const { logRequest } = require('../middleware/event.middleware.js') ;

const router = express.Router();
// log request using middleware when attempting to get students
router.get('/', logRequest, getStudents);
// log request using middleware when attempting to create student
router.post('/', logRequest, createStudent);
// log request using middleware when attempting to delete student
router.delete('/:id', logRequest, deleteStudent);
// export router
module.exports = router;