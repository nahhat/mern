const mongoose = require( 'mongoose');
const http = require( 'http');
const app = require( './app.js');
const dotenv  =require('dotenv');

// allow env variables
dotenv.config();

//  declare port to .env prt or 5000
const PORT = process.env.PORT || 5000;

// use node http server
const server = http.createServer(app);

// connect to Mongo db using mongoose
mongoose.connect(process.env.CONNECTION_URL, {
    useNewUrlParser:true, useUnifiedTopology:true
}).then(() => server.listen(PORT, () => 
    console.log(`Conection is estabilished and running on Port: ${PORT}`)
)).catch((err) => console.log(err.message));