const app = require('./app.js') ;
const request = require('supertest') ;

// test response status code of GET to /students
describe('test GET /students ', () =>{
    test('it should respond with status 200 success', async () =>{
        
        const response = await request(app).get('/students');
        expect(response.statusCode).toBe(200);
    });
});

// test POST requerst to /studnets
describe('test POST /students', () => {
    test('it should respond with status 201 success', async () =>{
        const response = await request(app).post('/students');
        expect(response.statusCode).toBe(201);
    });
});